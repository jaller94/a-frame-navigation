function t(arr: [number, number, number]): [number, number, number] {
    return [
        (arr[0]-120.288788) * -1000,
        (arr[1]-22.625633) * 1000,
        arr[2] / 30 - 4
    ];
}


async function main() {
    const res = await fetch('/data.json');
    const data = await res.json();
  
    const root = document.getElementById('root');
    for (const feature of data.features) {
        const tube = document.createElement('a-tube');
        const path = feature.geometry.coordinates.map(coord => {
            const pos = t(coord);
            return `${pos[0]} ${pos[2]} ${pos[1]}`;
        }).join(', ');
        console.log(path);
        tube.setAttribute('path', path);
        tube.setAttribute('segments', feature.geometry.coordinates.length);
        // tube.setAttribute('rotation', '0 45 0');
        tube.setAttribute('radius', `${0.05}`);
        tube.setAttribute('material', `color: blue`);
        root?.append(tube);

        for (let i = 0; i < feature.geometry.coordinates.length; i++) {
            const coordinate = feature.geometry.coordinates[i];
            const progress = i / feature.geometry.coordinates.length;
            const tube = document.createElement('a-box');
            const pos = t(coordinate);
            tube.setAttribute('position', `${pos[0]} ${pos[2]} ${pos[1]}`);
            // tube.setAttribute('rotation', '0 45 0');
            tube.setAttribute('width', `${0.1}`);
            tube.setAttribute('height', `${0.1}`);
            tube.setAttribute('depth', `${0.1}`);
            tube.setAttribute('color', `rgb(${Math.floor(progress*255)},0,0)`);
            root?.append(tube);
        }
        // for (let i = 0; i < feature.geometry.coordinates.length - 1; i++) {
        //     const coord1 = t(feature.geometry.coordinates[i]);
        //     const coord2 = t(feature.geometry.coordinates[i+1]);
    
        //     const center = [
        //         (coord1[0] + coord2[0]) / 2,
        //         (coord1[1] + coord2[1]) / 2,
        //         (coord1[2] + coord2[2]) / 2
        //     ];
    
        //     const A = new THREE.Vector3();
        //     const B = new THREE.Vector3();

        //     const dir = new THREE.Vector3();
        //     dir.subVectors( A, B ).normalize();
    
        //     A.set( coord1[0], coord1[1], coord1[2] );
        //     B.set( coord2[0], coord2[1], coord2[2] );
    
        //     const distance = A.distanceTo( B );
    
        //     console.log( center );
    
        //     const progress = i / feature.geometry.coordinates.length;
        //     const tube = document.createElement('a-box');
        //     tube.setAttribute('position', `${center[0]} ${center[2]} ${center[1]}`);
        //     tube.setAttribute('rotation', `${dir.x * 45} ${dir.z} 45`);
        //     tube.setAttribute('width', `${0.05}`);
        //     tube.setAttribute('height', `${0.05}`);
        //     tube.setAttribute('depth', `${distance}`);
        //     tube.setAttribute('color', `rgb(100,100,100)`);
        //     root?.append(tube);
        // }
    }
}
main();
