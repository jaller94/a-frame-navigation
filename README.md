# A-Frame Navigation

Render GPX tracks in 3D and VR with A-Frame.

## Build

The webapp with bundled JavaScript libraries will be put into the folder `out`.

```bash
bun install
bun run build
```

## Credits

* Map: OpenStreetMap contributors
* GPX Data: openrouteservice.org | OpenStreetMap contributors
